/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'ember-prj',
    environment: environment,
    contentSecurityPolicy: { 'connect-src': "'self' https://auth.firebase.com wss://*.firebaseio.com" },
    firebase: 'https://brilliant-heat-840.firebaseio.com/',
    baseURL: '/',
    locationType: 'auto',
    torii: {
      sessionServiceName: 'session',
      accessTokenName: 'token:GoogleOA2',
      providers: {
        'google-oauth2-bearer': {
          apiKey: '502205394464-s6ku4kc9b66dggu2ovu4n94m9ek33vl2.apps.googleusercontent.com',
          redirectUri: 'https://ember-1-benms.c9users.io/messages',
          // clientSecret: 'RVvbHWmx4gG2sI7b5k0Q6IVj',
        }
      },
    },
    flashMessageDefaults: {
      // flash message defaults
      timeout: 3000,
      extendedTimeout: 0,
      priority: 200,
      sticky: false,
      showProgress: true
    },
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },
    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
