import Ember from 'ember';
require("torii/load-initializers")['default']();

export default Ember.Route.extend({
  
    // flashMessages: Ember.inject.service(),
   
    actions: {
      logIn: function(){
          // this.get('flashMessages').success('OK sign in');
          return this.get('sposts').signIn();
      },
      logOut: function(){
          return this.get('sposts').signOut();
      }
  },
  torii: Ember.inject.service(),
  authManager: Ember.inject.service(),
  sposts: Ember.inject.service()
});
