import Ember from 'ember';

export default Ember.Route.extend({
    actions: {
        deleteMessage(message) {
            const sposts = this.get('sposts');
            if (confirm("Are you sure? You want to delete message?")) {
                sposts.removeMessage(message);
            }
        },
        editMessage(message) {
            this.transitionTo('messages.edit', message);
        },
        detailsMessage(message) {
            this.transitionTo('messages.message', message);
        },
        /*accessDenied() {
            return this.get('sposts').accessDenied();
        }*/
    },
    model() {
        const sposts = this.get('sposts');
        return sposts.getAllMessages();/*.then((arrModel)=>{
           return arrModel.toArray().reverse(); 
        });*/
    },
    afterModel(model) {
      return Ember.RSVP.all(model.invoke('get', 'profile'));
    },
    sposts: Ember.inject.service()
});
