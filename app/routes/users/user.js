import Ember from 'ember';

export default Ember.Route.extend({
    model(params) {
        const userId = params.user_id, 
            sposts = this.get('sposts');
        return Ember.RSVP.hash({
                user: sposts.getUserById(userId),
                messages: sposts.getMessagesByUser(userId)
            });
    },
    sposts: Ember.inject.service()
});
