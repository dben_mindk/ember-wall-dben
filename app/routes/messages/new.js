import Ember from 'ember';

export default Ember.Route.extend({
    actions: {
		createMessage(message) {
         const flashMessages = this.get('flashMessages');
           this.get('sposts').saveMessage(message);
           flashMessages.success('New message was successfully added');
           this.transitionTo('messages.message', message);
		}
	},
	model(){
		const store = this.get('sposts');
		return store.newMessage();
	},
	sposts: Ember.inject.service()
});
