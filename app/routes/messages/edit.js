import Ember from 'ember';

export default Ember.Route.extend({
    actions: {
		updateMessage(message) {
		  //console.log(message);
		  message.save();
		  this.transitionTo('messages.message', message);
		},
		cancelEditMessage(message) {
		  message.rollbackAttributes();
		  this.transitionTo('messages.message', message);
		}
	},
	model(params){
		const id = params.msg_id, 
		    store = this.get('sposts');
		return store.getMessageById(id);
	},
	sposts: Ember.inject.service()
});
