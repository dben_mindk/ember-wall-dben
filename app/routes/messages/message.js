import Ember from 'ember';

export default Ember.Route.extend({
    actions: {
        addBlahComment(message) {
            let comment = this.get('store').createRecord('comment', {
                text: 'blah-blah-blah',
                time: this.get('sposts').getCurrentDate(),
            });
            comment.set('message', message);
            // console.log(message);
            message.get('comments').addObject(comment).then((message) =>
              message.save()
            );
        },
        editComment(comment) {
            console.log('action edit comment');
        },
        deleteComment(comment) {
            //console.log('action edit comment');
            if (confirm("Are you sure? You want to delete comment?")) {
              return comment.destroyRecord();
            }
        },
    },
    model(params){
        const id = params.msg_id,
              sposts = this.get('sposts'),
              message = sposts.getMessageById(id);
              return message;
            /*return Ember.RSVP.hash({
                comments: sposts.getCommentsByMsgId(id),
                message: message
            });*/
    },
    sposts: Ember.inject.service()
 });
