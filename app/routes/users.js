import Ember from 'ember';

export default Ember.Route.extend({
    model() {
        const sposts = this.get('sposts');
        return sposts.getAllUsers();
    },
    sposts: Ember.inject.service()
});
