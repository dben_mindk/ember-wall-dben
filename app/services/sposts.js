import Ember from 'ember';

// import Message from 'wall/models/message';

export default Ember.Service.extend({
    // inject services
    _routing :        Ember.inject.service('-routing'),
    // authManager:      Ember.inject.service(),
    flashMessages:    Ember.inject.service(),
    session:          Ember.inject.service(),
    store:            Ember.inject.service(),
    
    getAllMessages() {
        const messages = this.get('store').query('message', 
        { orderBy: 'time', 
        //   limitToLast: 5
        }
       );
        return messages;
    },
    getMessageById(id) {
        console.log('getMessageById ('+id+')');
        // return this.getAllMessages().findBy('id', id);
        return this.get('store').findRecord('message', id);
    },
    getAllUsers() {
        return this.get('store').findAll('user');
    },
    getUserById(id) {
        // const users = this.getAllUsers();
        // return users.findBy('id', id);
        return this.get('store').findRecord('user', id);
    },
    getMessagesByUser(userId) {
        const messages = this.getAllMessages();
        messages.findBy('user', userId);
    },
    getCurrentDate(){
        return new Date().getTime();
    },
    newMessage() {
		return this.get('store').createRecord('message', {
		    title: '',
		    text: ''
		});
	},
	//new\edited message
	saveMessage(message){ 
	    // MChurilov user id
	    const userId = '105946079868681622491';
	    this.getUserById(userId).then((user) => {
	    message.set('time', this.getCurrentDate());
	    message.set('user', user);
	    message.save();
     });
	},
	removeMessage(message) {
	    // app/controllers/posts.js
        const deletions = message.get('comments').map(function(comment) {
          return comment.destroyRecord();
        });
        // Ensures all comments are deleted before the post
        Ember.RSVP.all(deletions)
          .then(function() {
          return message.destroyRecord();
          })
        .catch(function(e) {
          // Handle errors
          console.log('Error in service remove message');
          console.dir(e);
        });  
	},
    getAllComments() {
        return this.get('store').findAll('comment');
    },
    getCommentsByMsgId(msgId){
        const comments = this.getAllComments();
        return comments.findBy('message', msgId);
    },
    
    signIn(){
    //   return this.get('authManager').authenticate();
      this.get('session').open('google-oauth2-bearer')
       .then(() => {
            // console.log('sign in', this);
            this.get('flashMessages').success('Successfully signed in');
            this.get('_routing').transitionTo('messages');
       }, 
       function(error){
        this.get('flashMessages').error('Could not sign you in: ' + error.message);
        // Ember.controllerFor('about').set('error', 'Could not sign you in: ' + error.message);
        });    
    },
    
    signOut() {
      //   return this.get('authManager').invalidate();
      this.get('session').close('google-oauth2-bearer')
      .then(() => {
            // this.get('flashMessages').success('Comment was successfully deleted');
            this.get('flashMessages').success('Successfully signed out');
            this.get('_routing').transitionTo('about');
      }, 
       function(error){
        //   this.get('flashMessages').error('Could not sign you in: ' + error.message);
        //   Ember.controllerFor('about').set('error', 'Could not sign you in: ' + error.message);
        console.log('error', 'Could not sign you in: ' + error.message);
        }
      );
    },
    
    accessDenied() {
        this.get('_routing').transitionTo('about');
    }
}); 
