import Ember from 'ember';
import DS from 'ember-data';
import config from './../config/environment';


export default Ember.Service.extend({
    torii: Ember.inject.service(),
    username: null,
    accessToken: null,
    store: Ember.inject.service(),

    initializeFromCookie: function () {

        let username = Cookies.get('username');
        let accessToken = Cookies.get('accessToken');

        if (!!accessToken) {
            this.set('username', username);
            this.set('accessToken', accessToken);
        }
    }.on('init'),

    getUserInfo(response) {
        return new Promise(function (resolve, reject) {
            Ember.$.ajax({
                type: "GET",
                url: "https://www.googleapis.com/oauth2/v2/userinfo",
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", response.token_type + " " + response.access_token);
                },
                success: function (user) {
                    resolve(user);
                },
                error: function (error) {
                    reject(error);
                }
            });
        });
    },

    getAuthCode() {
        let self = this;
        return self.get('torii').open('google-oauth2').then(function (authorization) {
            return authorization.authorizationCode;
            // self.set('authorizationCode', authorization.authorizationCode);
            // Cookies.set('authorizationCode', authorization.authorizationCode);
        }, function (error) {
        });
    },

    getAccessToken(authorizationCode) {
        // let self = this;
        return $.post('https://www.googleapis.com/oauth2/v3/token',
            {
                code: authorizationCode,
                redirect_uri: config.torii.providers['google-oauth2'].redirectUri,
                client_id: config.torii.providers['google-oauth2'].apiKey,
                grant_type: 'authorization_code',
                client_secret: config.torii.providers['google-oauth2'].clientSecret,
            }).then(function (response) {
            return response;
        }, function (error) {
        });
    },

    authenticate() {
        let self = this;
        //Once you got the Authorization Code we have to Exchange authorization code for tokens,
        //you will get a refresh and an access token which is required to access OAuth protected resources.
        //with access token you can get user information depend on scope
        //each time if token is expired we should send request to refresh our token
        return new Ember.RSVP.Promise(function (resolve, reject) {
            self.getAuthCode().then(
                function (authorizationCode) {
                    self.getAccessToken(authorizationCode).then(
                        function (response) {
                            self.getUserInfo(response).then(function (user) {
                                if (user.hd == 'mindk.com') {
                                    self.set('username', user.name);
                                    self.set('photo', user.picture);
                                    self.set('accessToken', authorizationCode);
                                    Cookies.set('username', user.name);
                                    Cookies.set('accessToken', authorizationCode);
                                    resolve(user);
                                } else {
                                    reject('User should be from @mindk');
                                }
                            })
                        }
                    )
                }
            );
        });
    },

    invalidate() {
        this.set('accessToken', null);
        Cookies.remove('accessToken');
    },
    
    isAuthenticated: Ember.computed.bool('accessToken')

});