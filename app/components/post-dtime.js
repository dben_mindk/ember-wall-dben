import Ember from 'ember';

export default Ember.Component.extend({
ftime: Ember.computed('time', function() {
const date = new Date(this.get('time')), //*1000
      months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
      year = date.getFullYear(),
      month = months[date.getMonth()],
      day = date.getDate(),
      hours = date.getHours(),
      minutes = "0" + date.getMinutes(),
      // seconds = "0" + date.getSeconds(),
    formattedTime = hours + ':' + minutes.substr(-2) + ' ' + day + '/' + month + '/' +year;
    return formattedTime;
}), 
});
