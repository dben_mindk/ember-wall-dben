import Ember from 'ember';

export default Ember.Component.extend({
    actions: {
		submitMessage() {
		  const title = this.get('model.title'),
		  		body = this.get('model.text');
	  		
	  		if(title.length>0 && body.length>0){
				this.sendAction('action', title, body);
				// this.transitionTo('messages.message', model);
			}
			else{
				//Materialize.toast('Both post title and body need to be entered.',3000);
				alert('Both post title and body need to be entered.');
			}
		  //this.transitionTo('messages.message', message);
		}
	},
	model(){
		const store = this.get('sposts');
		return store.newMessage();
	},
	sposts: Ember.inject.service()    
});
