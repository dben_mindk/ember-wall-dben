import DS from 'ember-data';

export default DS.Model.extend({
  message: DS.belongsTo('message', {async: true}),
  text: DS.attr('string'),
  time: DS.attr('string'),
  user: DS.attr('string')
});

