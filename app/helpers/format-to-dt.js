import Ember from 'ember';

export function formatToDate(params/*, hash*/) {
const tStamp = params[0],
      date = new Date(tStamp), //*1000
      months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
      year = date.getFullYear(),
      month = months[date.getMonth()],
      day = date.getDate(),
      // hours = date.getHours(),
      // minutes = "0" + date.getMinutes(),
      // seconds = "0" + date.getSeconds(),
    formattedTime = day + '/' + month + '/' +year;
  return formattedTime;
}

export default Ember.Helper.helper(formatToDate);
