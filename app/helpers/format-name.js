import Ember from 'ember';

export function formatName(params/*, hash*/) {
  const name = params[0];
  return ((typeof(name) === 'string' && name.length) ? name : 'NoName');
}

export default Ember.Helper.helper(formatName);
