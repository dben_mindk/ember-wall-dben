import Ember from 'ember';

export function formatText(params/*, hash*/) {
   const name = params[0];
  return ((typeof(name) === 'string' && name.length) ? name : '(empty)');
}

export default Ember.Helper.helper(formatText);
